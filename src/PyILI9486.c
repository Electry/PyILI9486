#include <Python.h>
#include <bcm2835.h>
#include <sys/time.h>
#include <math.h>

#define ILI_SLPIN  0x10  // Sleep IN
#define ILI_SLPOUT 0x11  // Sleep OUT
#define ILI_DISPON 0x29  // Display ON

#define ILI_CASET  0x2A  // Column ADDR set
#define ILI_PASET  0x2B  // Row ADDR set
#define ILI_RAMWR  0x2C  // Write to RAM

#define ILI_MADCTL 0x36  // Memory Access Control
#define ILI_PIXFMT 0x3A  // Pixel Format

#define ILI_GMCTRP 0xE0  // Positive Gamma Control
#define ILI_GMCTRN 0xE1  // Negative Gamma Control

#define LCD_WIDTH  480
#define LCD_HEIGHT 320

#define MIN(a, b) ((a < b) ? a : b)

int delayms(int ms) {
    struct timespec tim, timr;
    tim.tv_sec = 0;
    tim.tv_nsec = (long)(ms * 1000000L);

    return nanosleep(&tim, &timr);
}

static PyObject *
PyILI9486_init(PyObject *self, PyObject *args)
{
    unsigned int port;
    unsigned int device;
    int is_rpi3 = 1;
    int ret;

    if (!PyArg_ParseTuple(args, "IIp", &port, &device, &is_rpi3)) {
        return NULL;
    }

    ret = bcm2835_init();
    if (!ret)
        return NULL;

    ret = bcm2835_spi_begin();
    if (!ret)
        return NULL;

    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST);
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    if (is_rpi3)
        bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_16);
    else
        bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_8);

    bcm2835_spi_chipSelect(device);
    bcm2835_spi_setChipSelectPolarity(device, LOW);

    return Py_BuildValue("i", ret);
}

static PyObject *
PyILI9486_close(PyObject *self)
{
   int ret = bcm2835_close();

   return Py_BuildValue("i", ret);
}


static void
ILI9486_send_cmd(unsigned int data)
{
    char a[4], b[4];
    memset(&a, 0, sizeof(a));
    memset(&b, 0, sizeof(b));

    a[1] = b[1] = data >> 8;
    a[2] = b[2] = data & 0x00FF;
    a[3] = 0x11;
    b[3] = 0x1B;

    bcm2835_spi_transfern(a, 4);
    bcm2835_spi_transfern(b, 4);
}

static void
ILI9486_send_data(unsigned int data)
{
    char a[4], b[4];
    memset(&a, 0, sizeof(a));
    memset(&b, 0, sizeof(b));

    a[1] = b[1] = data >> 8;
    a[2] = b[2] = data & 0x00FF;
    a[3] = 0x15;
    b[3] = 0x1F;

    bcm2835_spi_transfern(a, 4);
    bcm2835_spi_transfern(b, 4);
}

static PyObject *
PyILI9486_lcd_wake(PyObject *self)
{
    ILI9486_send_cmd(ILI_SLPOUT);

    Py_RETURN_NONE;
}

static PyObject *
PyILI9486_lcd_sleep(PyObject *self)
{
    ILI9486_send_cmd(ILI_SLPIN);

    Py_RETURN_NONE;
}

static PyObject *
PyILI9486_lcd_init(PyObject *self)
{
    int i;

    ILI9486_send_cmd(ILI_SLPOUT);
    delayms(200);

    ILI9486_send_cmd(ILI_MADCTL);
    ILI9486_send_data(0x28);

    ILI9486_send_cmd(ILI_PIXFMT);
    ILI9486_send_data(0x55);

    ILI9486_send_cmd(ILI_GMCTRP);
    for (i = 0; i < 15; i++)
        ILI9486_send_data(0xFF);

    ILI9486_send_cmd(ILI_GMCTRN);
    for (i = 0; i < 15; i++)
        ILI9486_send_data(0x00);

    ILI9486_send_cmd(ILI_DISPON);

    delayms(200);

    Py_RETURN_NONE;
    //return Py_BuildValue("i", ret);
}

static PyObject *
PyILI9486_lcd_setbounds(PyObject *self, PyObject *args)
{
    unsigned int x, y, w, h;

    if (!PyArg_ParseTuple(args, "IIII", &x, &y, &w, &h)) {
        return NULL;
    }

    ILI9486_send_cmd(ILI_CASET);
    ILI9486_send_data(x >> 8);
    ILI9486_send_data(0x00FF & x);
    ILI9486_send_data((x + w - 1) >> 8);
    ILI9486_send_data(0x00FF & (x + w - 1));

    ILI9486_send_cmd(ILI_PASET);
    ILI9486_send_data(y >> 8);
    ILI9486_send_data(0x00FF & y);
    ILI9486_send_data((y + h - 1) >> 8);
    ILI9486_send_data(0x00FF & (y + h - 1));

    ILI9486_send_cmd(ILI_RAMWR);

    Py_RETURN_NONE;
}

static PyObject *
PyILI9486_lcd_setpixels(PyObject *self, PyObject *args)
{
    char *data;
    int len, i;
    int r, g, b, rgb;

    if (!PyArg_ParseTuple(args, "s#", &data, &len)) {
        return NULL;
    }

    for (i = 0; i < len; i += 3) {
        r = MIN(31, (int)round(data[i] / 8));
        g = MIN(31, (int)round(data[i+1] / 4));
        b = MIN(31, (int)round(data[i+2] / 8));
        rgb = (r << 11) + (g << 5) + b;
        ILI9486_send_data(rgb);
    }

    return Py_BuildValue("i", i);
}

static PyMethodDef PyILI9486_Methods[] = {
    {"init", (PyCFunction)PyILI9486_init, METH_VARARGS, "Initialize ILI9486 library."},
    {"close", (PyCFunction)PyILI9486_close, METH_NOARGS, "Close ILI9486 library."},

    {"lcd_init", (PyCFunction)PyILI9486_lcd_init, METH_NOARGS, "Prepare LCD display."},

    {"lcd_sleep", (PyCFunction)PyILI9486_lcd_sleep, METH_NOARGS, "Sleep IN"},
    {"lcd_wake", (PyCFunction)PyILI9486_lcd_wake, METH_NOARGS, "Sleep OUT"},

    {"lcd_setbounds", (PyCFunction)PyILI9486_lcd_setbounds, METH_VARARGS, "Set write bounds."},
    {"lcd_setpixels", (PyCFunction)PyILI9486_lcd_setpixels, METH_VARARGS, "Write RGB565 pixels on LCD."},

    {NULL, NULL, 0, NULL} /* Sentinel */
};

struct module_state {
    PyObject *error;
};

static int PyILI9486_Traverse(PyObject *m, visitproc visit, void *arg) {
    Py_VISIT(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static int PyILI9486_Clear(PyObject *m) {
    Py_CLEAR(((struct module_state*)PyModule_GetState(m))->error);
    return 0;
}

static struct PyModuleDef PyILI9486_Module = {
    PyModuleDef_HEAD_INIT,
    "PyILI9486",
    NULL,
    sizeof(struct module_state),
    PyILI9486_Methods,
    NULL,
    PyILI9486_Traverse,
    PyILI9486_Clear,
    NULL
};

PyMODINIT_FUNC
PyInit_PyILI9486(void)
{
    PyObject *m;

    m = PyModule_Create(&PyILI9486_Module);
    if (m == NULL)
        return NULL;

    struct module_state *st = ((struct module_state*)PyModule_GetState(m));

    st->error = PyErr_NewException("PyILI9486.error", NULL, NULL);
    if (st->error != NULL) {
        Py_DECREF(m);
    }

    PyModule_AddObject(m, "error", st->error);
    return m;
}
