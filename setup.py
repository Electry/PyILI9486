from distutils.core import setup, Extension

pyili9486 = Extension('PyILI9486',
                      include_dirs=['/usr/local/include'],
                      libraries = ['bcm2835'],
                      library_dirs = ['/usr/local/lib'],
                      sources = ['src/PyILI9486.c'])

setup (name = 'PyILI9486',
       version = '0.0.1',
       description = 'Python extension for ILI9486 based TFT displays.',
       author='Michal Chvila',
       author_email='michal@chvila.sk',
       license='GPLv2',
       keywords=["raspberry pi", "bcm2835", "ili9486"],
       classifiers=[
           "Programming Language :: Python",
           "Development Status :: 3 - Alpha",
           "Topic :: System :: Hardware"
       ],
       ext_modules = [pyili9486])
